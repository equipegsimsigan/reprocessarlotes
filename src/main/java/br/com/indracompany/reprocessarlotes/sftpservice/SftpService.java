package br.com.indracompany.reprocessarlotes.sftpservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import br.com.indracompany.reprocessarlotes.utils.Constants;
import br.com.indracompany.reprocessarlotes.utils.LogUtil;

public class SftpService {
	public static Vector<?> getConnectionFileZillaGSIMHOMOL(String dir) {

		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");

		// boolean connected = false;
		Vector list = null;

		try {

			JSch jsch = new JSch();
			Session session = jsch.getSession(Constants.GSIM_Server_User, Constants.GSIM_Server);
			session.setPassword(Constants.GSIM_Server_Pass);
			session.setConfig(config);
			session.connect();
			ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
			channelSftp.connect();

			try {
				list = channelSftp.ls(dir);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (session.isConnected()) {
				LogUtil.info("Conectado em Homol");
			}
		} catch (JSchException e) {
			e.printStackTrace();
			LogUtil.info("Não foi possivel conectar no servidor do GSIM");
		}

		LogUtil.info("Procurando lotes no diretório " + dir);
		return list;
	}

	public static Vector<?> getConnectionFileZillaGSIMPROD(String dir) {

		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");

		Vector list = null;

		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(Constants.GSIM_Server_User, Constants.GSIM_Server);
			session.setPassword(Constants.GSIM_Server_Pass);
			session.setConfig(config);
			session.connect();
			ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
			channelSftp.connect();

			try {
				list = channelSftp.ls(dir);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (session.isConnected()) {
				LogUtil.info("Conectado em Prod");
			}
		} catch (JSchException e) {
			e.printStackTrace();
			LogUtil.error("Não foi possivel conectar no ambiente de prod", e);
		}
		LogUtil.info("Procurando lotes no diretório " + dir);
		return list;
	}

	public static void downloadFiles(String source, String destination) throws JSchException, SftpException {

		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");

		JSch jsch = new JSch();
		Session session = jsch.getSession(Constants.GSIM_Server_User, Constants.GSIM_Server);
		session.setPassword(Constants.GSIM_Server_Pass);
		session.setConfig(config);
		session.connect();
		ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
		channelSftp.connect();
		channelSftp.get(source, destination);

	}

	public static ArrayList<File> renameFiles() {
		File file = new File(Constants.FILE_OUTPUTFILE);
		ArrayList<File> newFiles = new ArrayList<>();
		int qtdRename = 0;
		// /outputfile
		File[] arquivos = file.listFiles();
		for (File verificaErro : arquivos) {
			if (verificaErro.getName().contains("erro")) {
				File file1 = new File(Constants.FILE_OUTPUTFILE,
						"VIVO0".concat(verificaErro.getName()).substring(5, 16).concat(".out.pgp"));
				boolean rename = false;
				rename = verificaErro.renameTo(file1);
				if (rename) {
					qtdRename++;
					newFiles.add(file1);
				} else {
					verificaErro.delete();
				}

			}
		}
		if (newFiles.size() > 0) {
			LogUtil.info("Arquivos renomeados : " + newFiles.size());
		}
		return newFiles;
	}

	public static void descompactFiles() throws FileNotFoundException, IOException {
		File file = new File(Constants.FILE_OUTPUTFILE);
		int count = 0;
		// /outputfile
		File[] arquivos = file.listFiles();
		for (File verificaErro : arquivos) {
			final int TAMANHO_BUFFER = 2048;
			byte[] dados = new byte[TAMANHO_BUFFER];
			if (verificaErro.getName().contains("gz")) {
				try (GZIPInputStream in = new GZIPInputStream(
						new FileInputStream("./outputfile" + "/" + verificaErro.getName()))) {
					try (FileOutputStream out = new FileOutputStream(
							"./outputfile" + "/" + verificaErro.getName().replace("gz", ""))) {

						int bytesLidos;
						while ((bytesLidos = in.read(dados, 0, TAMANHO_BUFFER)) > 0) {
							out.write(dados, 0, bytesLidos);
						}
						count++;
						out.close();
						in.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					LogUtil.error("Arquivo gz vazio", e);
					e.printStackTrace();
				}

			} else {

			}
		}
		LogUtil.info(count + " arquivo(s) descompactado(s)");
	}

	public static void uploadFiles(String source, String destination) throws JSchException, SftpException {

		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch jsch = new JSch();
		Session session = jsch.getSession(Constants.GSIM_Server_User, Constants.GSIM_Server);
		session.setPassword(Constants.GSIM_Server_Pass);
		session.setConfig(config);
		session.connect();
		Channel channel = session.openChannel("sftp");
		channel.connect();
		ChannelSftp sftpChannel = (ChannelSftp) channel;
		sftpChannel.put(source, destination);
	}
}

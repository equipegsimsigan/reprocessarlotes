package br.com.indracompany.reprocessarlotes.sftpservice;

//import static br.com.indracompany.utils.LogUtils.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.indracompany.reprocessarlotes.models.Lotes;
import br.com.indracompany.reprocessarlotes.models.Response;
import br.com.indracompany.reprocessarlotes.repository.LotesRepository;
import br.com.indracompany.reprocessarlotes.repository.SimCardRepository;
import br.com.indracompany.reprocessarlotes.soap.SoapWebService;
import br.com.indracompany.reprocessarlotes.utils.Constants;
import br.com.indracompany.reprocessarlotes.utils.LogUtil;

@Service
public class FileZillaService {

	@Autowired
	SimCardRepository scr;

	@Autowired
	LotesRepository lr;

	public ResponseEntity<Response> fileZillaStart(List<String> lote) {
		Response response = new Response();
		ArrayList<String> lotesUpload = new ArrayList<>();

		try {
			List<String> lotesEncontradosErro = new ArrayList<>();
			List<String> lotesProcurarExpurgo = new ArrayList<>();
			List<String> lotesEncontradosExpurgo = new ArrayList<>();
			List<String> lotesEncontradosGeral = new ArrayList<>();
			List<String> listaLotes = new ArrayList<>();

			// Procura os lotes no banco de dados
			listaLotes = scr.findLotes(lote);

			// Procura os lotes do banco na pasta de erro
			lotesEncontradosErro = ProcessService.buscarLotesPastaErro(listaLotes);

			for (String lotesErro : lotesEncontradosErro) {
				if (lotesErro.startsWith("VIVO")) {
					lotesEncontradosGeral.add(lotesErro);
				}
			}

			// Organiza os lotes que ainda não foram encontrados para procurar na expurgo
			for (String lotesEncontradoErro : lotesEncontradosErro) {
				if (!lotesEncontradoErro.startsWith("VIVO")) {
					lotesProcurarExpurgo.add(Constants.INICIAL_NAME_OUTPUTFILE.concat(lotesEncontradoErro)
							.concat(Constants.FINAL_NAME_OUTPUTFILE));
				}
			}

			LogUtil.info("Faltam " + lotesProcurarExpurgo.size() + " outputfile a ser procurado(s)");

			LogUtil.info("Buscando lotes na pasta de expurgo");
			lotesEncontradosExpurgo = ProcessService.buscarLotesPastaExpurgo(lotesProcurarExpurgo);

			// Adiciona na lista os lotes que foram encontrados na pasta de expurgo
			for (String lotesExpurgo : lotesEncontradosExpurgo) {
				if (lotesExpurgo.startsWith("VIVO")) {
					lotesEncontradosGeral.add(lotesExpurgo);
				}
			}

			// Notifica os lotes não encontrados
			for (String notFound : lotesProcurarExpurgo) {
				LogUtil.info(notFound + " não encontrado");
			}

			// Descompacta, renomeia e sobe os arquivos pro servidor
			LogUtil.info("Procurando lotes a serem descompactados");
			SftpService.descompactFiles();
			SftpService.renameFiles();
			File file = new File(Constants.FILE_OUTPUTFILE);
			File[] arquivos = file.listFiles();
			int qtdUpload = 0;
			for (File verificaErro : arquivos) {
				if (verificaErro.isFile() && verificaErro.getName().endsWith("pgp")) {
					SftpService.uploadFiles(verificaErro.getAbsolutePath(),
							Constants.ENTRADA + "/" + verificaErro.getName());
					qtdUpload++;
					response.setMensagem("Lote processado no FileZilla");
					lotesUpload.add(verificaErro.getName());
				} else {
					response.setMensagem("Não foi possível processar o lote " + verificaErro.getName());
				}
			}
			LogUtil.info("Foram inseridos " + qtdUpload + " outputfile na pasta de entrada");
		} catch (Exception e) {
			LogUtil.error("Ocorreu uma falha durante a execução!", e);
			response.setMensagem("Não foi possível processar o lote");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
		response.setLotesUpload(lotesUpload);

		List<String> lotesUploadEntrada = new ArrayList<>();
		List<String> ReceiveOutputfile = new ArrayList<>();
		
		boolean exist = false;

		LogUtil.info("Buscando lotes para atualizar no banco");
		for (String lotesFindQueue : lotesUploadEntrada) {
			exist = lr.findLotesQueue(lotesFindQueue);
			if (exist) {
				lr.updateImportQueue(lotesFindQueue);
				ReceiveOutputfile.add(lotesFindQueue);
			} else {
				Lotes lotes = new Lotes();
				lotes.setFileName(lotesFindQueue);
				lotes.setDateNew(LocalDateTime.now());
				lotes.setQueuePosition("01");
				lr.save(lotes);
				ReceiveOutputfile.add(lotesFindQueue);
			}

		}

		LogUtil.info("Realizando a chamada do serviço ReceiveOutputFile para " + ReceiveOutputfile.size() + " lotes");

		for (String outputfile : ReceiveOutputfile) {
			SoapWebService.callSoapWebServiceGSIM(outputfile, Constants.endpointGSIMPROD);
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
}

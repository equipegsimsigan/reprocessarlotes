package br.com.indracompany.reprocessarlotes.sftpservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import br.com.indracompany.reprocessarlotes.utils.Constants;
import br.com.indracompany.reprocessarlotes.utils.LogUtil;

public class ProcessService {
	public static List<String> buscarLotesPastaErro(List<String> listaLotes) {
		List<String> lotesPastaErro = listaLotes;

		Vector<?> filesServer = new Vector();

		// Lista arquivos e procura lotes presos em fabricacao na pasta de erro
		try {
			int qtd = 0;
			if (!lotesPastaErro.isEmpty()) {
				filesServer = SftpService.getConnectionFileZillaGSIMHOMOL(Constants.ERRO);

				for (int i = 0; i < filesServer.size(); i++) {
					ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry) filesServer.get(i);

					for (int n = 0; n < lotesPastaErro.size(); n++) {
						String outputfileErro = lsEntry.getFilename();
						if (outputfileErro.contains("_")) {
							String[] erro = outputfileErro.split("_");
							if (erro[2].contains("erro")) {
								if (outputfileErro.substring(5, 11).equals(lotesPastaErro.get(n))) {
									String output = lsEntry.getFilename().substring(5, 11);
									if (!lotesPastaErro.contains(outputfileErro)) {
										SftpService.downloadFiles(Constants.ERRO + "/" + lsEntry.getFilename(),
												Constants.FILE_OUTPUTFILE + "/" + lsEntry.getFilename());
										LogUtil.info("Arquivo " + lsEntry.getFilename() + " encontrado");
										lotesPastaErro.add(lsEntry.getFilename());
										lotesPastaErro.remove(output);
										qtd++;
									}
								}
							}
						} else if (!outputfileErro.contains("_")) {
							if (!lotesPastaErro.get(n).startsWith("VIVO")) {
								String outputfile = Constants.INICIAL_NAME_OUTPUTFILE
										.concat(lotesPastaErro.get(n).concat(Constants.FINAL_NAME_OUTPUTFILE));
								if (lsEntry.getFilename().equals(outputfile)) {
									if (!lotesPastaErro.contains(outputfile)) {
										SftpService.downloadFiles(Constants.ERRO + "/" + lsEntry.getFilename(),
												Constants.FILE_OUTPUTFILE + "/" + lsEntry.getFilename());
										LogUtil.info("Arquivo " + lsEntry.getFilename() + " encontrado");
										lotesPastaErro.add(lsEntry.getFilename());
										lotesPastaErro.remove(n);
										qtd++;
									}
								}
							}

						} else if (outputfileErro.contains("Virtual")) {
							if (lsEntry.getFilename().contains(("VIVOVirtual" + lotesPastaErro.get(n)))) {
								SftpService.downloadFiles(Constants.ERRO + "/" + lsEntry.getFilename(),
										Constants.FILE_OUTPUTFILE + "/" + lsEntry.getFilename());
								LogUtil.info("Arquivo " + lsEntry.getFilename() + " encontrado");
								lotesPastaErro.add(lsEntry.getFilename());
								lotesPastaErro.remove(n);
							}
						}
					}
				}
				LogUtil.info("Foram encontrados e baixados " + qtd + " outputfile da pasta de erro");
			} else
				LogUtil.info("Lotes inexistentes");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lotesPastaErro;

	}

	public static List<String> buscarLotesPastaExpurgo(List<String> lotes)
			throws JSchException, SftpException {
		List<String> lotesPastaExpurgo = lotes;
		List<String> lotesNovos = new ArrayList<>();
		Vector<?> filesServer = new Vector();

		// Procura lotes presos em fabricacao na pasta de expurgo
		try {

			if (!lotesPastaExpurgo.isEmpty()) {
				filesServer = SftpService.getConnectionFileZillaGSIMHOMOL(Constants.EXPURGO);

				for (int i = 0; i < filesServer.size(); i++) {

					ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry) filesServer.get(i);

					for (int n = 0; n < lotesPastaExpurgo.size(); n++) {
						if (!lsEntry.getFilename().startsWith(".")) {
							String a = lotesPastaExpurgo.get(n).substring(5, 11);
							if (lsEntry.getFilename().substring(5, 11).equals(a)) {
								if (!lotesNovos.contains(lotesPastaExpurgo.get(n))) {
									SftpService.downloadFiles(Constants.EXPURGO + "/" + lsEntry.getFilename(),
											Constants.FILE_OUTPUTFILE + "/" + lsEntry.getFilename());
									LogUtil.info("Arquivo " + lsEntry.getFilename() + " encontrado");
									lotesNovos.add(lsEntry.getFilename());
									lotesPastaExpurgo.remove(lotesPastaExpurgo.get(n));
								}
							}
						}

					}
				}

				LogUtil.info("Foram encontrados e baixados " + lotesNovos.size() + " outputfile da pasta de expurgo");

			} else {
				System.out.println("Lotes inexistentes");
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return lotesNovos;
	}
}

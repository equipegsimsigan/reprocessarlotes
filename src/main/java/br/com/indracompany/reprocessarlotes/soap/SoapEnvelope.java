package br.com.indracompany.reprocessarlotes.soap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import br.com.indracompany.reprocessarlotes.utils.Constants;

public class SoapEnvelope {
	public static void createSoapEnvelopeGSIM(String in0, SOAPMessage soapMessage) throws SOAPException {

		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope envelope = soapPart.getEnvelope();
		SOAPHeader soapHead = soapMessage.getSOAPHeader();
		SOAPBody soapBody = envelope.getBody();

		// SOAP Envelope
		envelope.addNamespaceDeclaration(Constants.WS, Constants.prefixGSIM);

		// SOAP Body
		SOAPElement soapBodyElem = soapBody.addChildElement("receiveOutputFile", Constants.WS);
		soapBodyElem.addChildElement("in0", Constants.WS).addTextNode(in0);
		soapBodyElem.addChildElement("in1", Constants.WS).addTextNode(Constants.GSIM_User);
		soapBodyElem.addChildElement("in2", Constants.WS).addTextNode(Constants.GSIM_Pass);

		envelope.setPrefix("soapenv");
		soapHead.setPrefix("soapenv");
		soapBody.setPrefix("soapenv");
		envelope.removeNamespaceDeclaration("SOAP-ENV");

	}
}

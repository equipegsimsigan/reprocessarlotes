package br.com.indracompany.reprocessarlotes.soap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPMessage;

public class SoapRequest {
	SOAPMessage createSOAPRequestGSIM(String in0) throws Exception {

		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		SoapEnvelope.createSoapEnvelopeGSIM(in0, soapMessage);
		soapMessage.saveChanges();
		System.out.println("Chamada do serviço ReceiveOutputFileWS\n");
		soapMessage.writeTo(System.err);

		return soapMessage;
	}

}

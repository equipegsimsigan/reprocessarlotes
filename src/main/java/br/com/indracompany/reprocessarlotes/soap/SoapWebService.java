package br.com.indracompany.reprocessarlotes.soap;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

public class SoapWebService {
	public static SOAPMessage callSoapWebServiceGSIM(String in0, String soapEndpointUrl) {

		SOAPMessage soapResponse = null;

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			SoapRequest request = new SoapRequest();

			// Send SOAP Message to SOAP Server
			soapResponse = soapConnection.call(request.createSOAPRequestGSIM(in0), soapEndpointUrl);

		} catch (Exception e) {
			System.err.println(
					"\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
			e.printStackTrace();
		}
		return soapResponse;
	}
}

package br.com.indracompany.reprocessarlotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

import br.com.indracompany.reprocessarlotes.repository.LotesRepository;
import br.com.indracompany.reprocessarlotes.repository.SimCardRepository;

@SpringBootApplication
public class ReprocessarLotesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReprocessarLotesApplication.class, args);
	}

}

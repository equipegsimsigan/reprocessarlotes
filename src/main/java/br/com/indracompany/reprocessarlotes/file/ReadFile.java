package br.com.indracompany.reprocessarlotes.file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class ReadFile {
	public static final HashMap<String, String> getFileInfo() {

		HashMap<String, String> keyValues = new HashMap<String, String>();
		BufferedReader br = null;
		String line = null;

		try {
			br = new BufferedReader(new FileReader("constants.txt"));

			while ((line = br.readLine()) != null) {
				// Constant name and Value is separated by space
				keyValues.put(line.split("=")[0], line.split("=")[1]);
			}

		} catch (IOException ie) {
			ie.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return keyValues;
	}

}

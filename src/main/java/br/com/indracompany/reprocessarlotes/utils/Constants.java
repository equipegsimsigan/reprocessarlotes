package br.com.indracompany.reprocessarlotes.utils;

import java.util.HashMap;

import br.com.indracompany.reprocessarlotes.file.ReadFile;

public class Constants {
	// Status Lote
	public static final String LOTE_FABRICACAO = "01";

	// Informações do arquivo
	private static HashMap<String, String> fileInfo = ReadFile.getFileInfo();

	// GSIM
	public static String GSIM_User = fileInfo.get("GSIMUSER");
	public static String GSIM_Pass = fileInfo.get("GSIMPASS");

	// FILEZILLA
	public static String GSIM_Server = fileInfo.get("FILEZILLASERVER");
	public static String GSIM_Server_User = fileInfo.get("FILEZILLAUSER");
	public static String GSIM_Server_Pass = fileInfo.get("FILEZILLAPASS");

	// BANCO DE DADOS
	public static String GSIM_DB_Filter = fileInfo.get("FILTER");
	public static String GSIM_DB_Port = fileInfo.get("PORT");
	public static String GSIM_DB_Server = fileInfo.get("SERVER");
	public static String GSIM_DB_DataBase = fileInfo.get("DATABASE");
	public static String GSIM_DB_User = fileInfo.get("DATABASEUSER");
	public static String GSIM_DB_Pass = fileInfo.get("DATABASEPASS");

	// IMPORT_QUEUE
	public static final String QUEUE_POSITION = "1";

	// GSIM SOAP
	public static String endpointGSIMHOMOL = "http://10.129.178.44:7040/vivosim/services/ReceiveOutputFileWS";
	public static String endpointGSIMPROD = "http://10.238.8.15:7002/vivosim/services/WsAlteraStatusSimcard";
	public static String prefixGSIM = "http://ws.pub.card.vivosim.indra.es";
	public static String WS = "ws";

	// DIRECTOY FILEZILLA
	public static final String ENTRADA = "/opt/integracao/outputfile/entrada";
	public static final String EXPURGO = "/opt/integracao/outputfile/erro/expurgo";
	public static final String ERRO = "/opt/integracao/outputfile/erro";

	// NAME STRUCTURE
	public static final String INICIAL_NAME_OUTPUTFILE = "VIVO0";
	public static final String FINAL_NAME_OUTPUTFILE = ".out.pgp";

	// PATH FILE
	public static String FILE_OUTPUTFILE = "./outputfile";
}

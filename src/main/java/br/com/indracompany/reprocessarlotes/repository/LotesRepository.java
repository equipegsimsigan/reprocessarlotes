package br.com.indracompany.reprocessarlotes.repository;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.indracompany.reprocessarlotes.models.Lotes;

@Repository
public interface LotesRepository extends JpaRepository<Lotes, String> {

	@Query("select lt from Lotes lt where fileName = :lote")
	boolean findLotesQueue(String lote);

	@Modifying
	@Query("update Lotes set queuePosition = '01'" 
			+ " where fileName = :lote")
	boolean updateImportQueue(String lote);

//	@Query("insert into Lotes values(:lote, '01', sysdate, null, null")
//	boolean insertImportQueue(String lote);

}

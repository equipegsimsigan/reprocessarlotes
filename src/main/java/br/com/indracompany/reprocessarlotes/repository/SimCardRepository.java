package br.com.indracompany.reprocessarlotes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.indracompany.reprocessarlotes.models.SimCard;

@Repository
public interface SimCardRepository extends JpaRepository<SimCard, String> {
	
	@Query(" select distinct r.idRemoteEnableLotOrder from SimCard s, SimCardStatus st, RemoteEnableLotOrder r where"
			+ " r.idRemoteEnableLotOrder = s.idRemoteEnableLotOrder and"
			+ " st.statusCode = s.statusCode and"
			+ " s.idRemoteEnableLotOrder in (:lote)")
	List<String> findLotes(List<String> lote);
	
}

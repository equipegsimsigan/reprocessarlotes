package br.com.indracompany.reprocessarlotes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.indracompany.reprocessarlotes.models.Lotes;
import br.com.indracompany.reprocessarlotes.models.Response;
import br.com.indracompany.reprocessarlotes.models.SimCard;
import br.com.indracompany.reprocessarlotes.repository.LotesRepository;
import br.com.indracompany.reprocessarlotes.repository.SimCardRepository;
import br.com.indracompany.reprocessarlotes.sftpservice.FileZillaService;

@RestController
@RequestMapping("/lotes")
public class LotesController {

	@Autowired
	LotesRepository lr;
	
	@Autowired
	SimCardRepository sr;

	@Autowired
	FileZillaService fzs;
	
	@GetMapping(produces = "application/json")
	public @ResponseBody Iterable<SimCard> verifyLotes() {
		List<SimCard> lotesEncontrados = sr.findAll();
		return lotesEncontrados;
	}
	
	@PostMapping
	public @ResponseBody ResponseEntity<Response> verifyLotes(@RequestBody List<String> lote) { 
		return fzs.fileZillaStart(lote);
	}

}

package br.com.indracompany.reprocessarlotes.models;

import java.util.List;

public class Response {

	private String mensagem;

	private List<String> lotesUpload;
	
	public List<String> getLotesUpload() {
		return lotesUpload;
	}

	public void setLotesUpload(List<String> lotesUpload) {
		this.lotesUpload = lotesUpload;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}

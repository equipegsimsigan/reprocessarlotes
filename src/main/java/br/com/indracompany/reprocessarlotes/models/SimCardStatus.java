package br.com.indracompany.reprocessarlotes.models;

	import java.time.LocalDateTime;

	import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
	import javax.persistence.Table;

	@Entity
	@Table(name="simcard_status")
	public class SimCardStatus {
		
		@Id
		@Column(name="status_code")
		private String statusCode;
		
		@Column(name="status_description")
		private String statusDescription;
		
		@Column(name="usr_new")
		private String userNew;
		
		@Column(name="date_new")
		private LocalDateTime dateNew;
		
		@Column(name="usr_update")
		private String userUpdate;
		
		@Column(name="external_code")
		private String externalCode;
		

		public String getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}

		public String getStatusDescription() {
			return statusDescription;
		}

		public void setStatusDescription(String statusDescription) {
			this.statusDescription = statusDescription;
		}

		public String getUserNew() {
			return userNew;
		}

		public void setUserNew(String userNew) {
			this.userNew = userNew;
		}

		public LocalDateTime getDateNew() {
			return dateNew;
		}

		public void setDateNew(LocalDateTime dateNew) {
			this.dateNew = dateNew;
		}

		public String getUserUpdate() {
			return userUpdate;
		}

		public void setUserUpdate(String userUpdate) {
			this.userUpdate = userUpdate;
		}

		public String getExternalCode() {
			return externalCode;
		}

		public void setExternalCode(String externalCode) {
			this.externalCode = externalCode;
		}

}

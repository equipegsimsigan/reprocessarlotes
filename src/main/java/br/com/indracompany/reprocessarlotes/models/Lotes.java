package br.com.indracompany.reprocessarlotes.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.boot.autoconfigure.domain.EntityScan;

@javax.persistence.Entity
@EntityScan
@Table(name = "output_file_import_queue")
public class Lotes {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id_file")
	private int idFile;

	@Column(name = "file_name")
	private String fileName;

	@Column(name = "queue_position")
	private String queuePosition;

	@Column(name = "date_new")
	private LocalDateTime dateNew;

	@Column(name = "usr_update")
	private String userUpdate;

	@Column(name = "date_update")
	private String dateUpdate;
	

	public int getIdFile() {
		return idFile;
	}

	public void setIdFile(int idFile) {
		this.idFile = idFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getQueuePosition() {
		return queuePosition;
	}

	public void setQueuePosition(String queuePosition) {
		this.queuePosition = queuePosition;
	}


	public LocalDateTime getDateNew() {
		return dateNew;
	}

	public void setDateNew(LocalDateTime dateNew) {
		this.dateNew = dateNew;
	}

	public String getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(String userUpdate) {
		this.userUpdate = userUpdate;
	}

	public String getDateUpdate() {
		return dateUpdate;
	}

	public void setDateUpdate(String dateUpdate) {
		this.dateUpdate = dateUpdate;
	}
	
}

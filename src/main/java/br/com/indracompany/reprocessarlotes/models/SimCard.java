package br.com.indracompany.reprocessarlotes.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Id;


@javax.persistence.Entity
public class SimCard {

	@Id
	@Column(name = "id_simcard")
	private Integer idSimCard;

	private long iccid;

	@Column(name = "id_input_file")
	private Integer idInputFile;

	private long imsi;

	private long uimid;

	private Integer pin1;

	private long puk1;

	private Integer pin2;

	private long puk2;

	private String ki;

	private String akey;

	private String epwd;

	private long adm1;

	private long adm2;

	@Column(name = "status_code")
	private String statusCode;

	@Column(name = "usr_new")
	private String userNew;

	@Column(name = "date_new")
	private LocalDateTime dateNew;

	@Column(name = "imsi_roaming")
	private long imsiRoaming;

	private long msisdn;

	@Column(name = "id_hlr")
	private Integer idHlr;

	@Column(name = "kit_type")
	private Integer kitType;

	@Column(name = "id_mnc")
	private Integer idMnc;

	@Column(name = "id_simcard_test")
	private Integer idSimCardTest;

	@Column(name = "id_sap_product_code")
	private Integer idSapProductCode;

	@Column(name = "id_iccid_pattern")
	private Integer idIccidPattern;

	private Integer location;

	@Column(name = "id_denatran_lot_order")
	private Integer idDenatranLotOrder;

	@Column(name = "id_remote_enable_lot_order")
	private Integer idRemoteEnableLotOrder;

	private Integer opc;

	private Integer card;

	@Column(name = "virtual_network")
	private Integer virtualNetwork;

	private Integer combo;

	@Column(name = "serial_recharge")
	private Integer serialRecharge;

	private Integer pin;

	private Integer active;

	@Column(name = "id_instances")
	private Integer idInstances;

	private Integer eid;

	public Integer getIdSimCard() {
		return idSimCard;
	}

	public void setIdSimCard(Integer idSimCard) {
		this.idSimCard = idSimCard;
	}

	public long getIccid() {
		return iccid;
	}

	public void setIccid(long iccid) {
		this.iccid = iccid;
	}

	public Integer getIdInputFile() {
		return idInputFile;
	}

	public void setIdInputFile(Integer idInputFile) {
		this.idInputFile = idInputFile;
	}

	public long getImsi() {
		return imsi;
	}

	public void setImsi(long imsi) {
		this.imsi = imsi;
	}

	public long getUimid() {
		return uimid;
	}

	public void setUimid(long uimid) {
		this.uimid = uimid;
	}

	public Integer getPin1() {
		return pin1;
	}

	public void setPin1(Integer pin1) {
		this.pin1 = pin1;
	}

	public long getPuk1() {
		return puk1;
	}

	public void setPuk1(long puk1) {
		this.puk1 = puk1;
	}

	public Integer getPin2() {
		return pin2;
	}

	public void setPin2(Integer pin2) {
		this.pin2 = pin2;
	}

	public long getPuk2() {
		return puk2;
	}

	public void setPuk2(long puk2) {
		this.puk2 = puk2;
	}

	public String getKi() {
		return ki;
	}

	public void setKi(String ki) {
		this.ki = ki;
	}

	public String getAkey() {
		return akey;
	}

	public void setAkey(String akey) {
		this.akey = akey;
	}

	public String getEpwd() {
		return epwd;
	}

	public void setEpwd(String epwd) {
		this.epwd = epwd;
	}

	public long getAdm1() {
		return adm1;
	}

	public void setAdm1(long adm1) {
		this.adm1 = adm1;
	}

	public long getAdm2() {
		return adm2;
	}

	public void setAdm2(long adm2) {
		this.adm2 = adm2;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getUserNew() {
		return userNew;
	}

	public void setUserNew(String userNew) {
		this.userNew = userNew;
	}

	public LocalDateTime getDateNew() {
		return dateNew;
	}

	public void setDateNew(LocalDateTime dateNew) {
		this.dateNew = dateNew;
	}

	public long getImsiRoaming() {
		return imsiRoaming;
	}

	public void setImsiRoaming(long imsiRoaming) {
		this.imsiRoaming = imsiRoaming;
	}

	public long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(long msisdn) {
		this.msisdn = msisdn;
	}

	public Integer getIdHlr() {
		return idHlr;
	}

	public void setIdHlr(Integer idHlr) {
		this.idHlr = idHlr;
	}

	public Integer getKitType() {
		return kitType;
	}

	public void setKitType(Integer kitType) {
		this.kitType = kitType;
	}

	public Integer getIdMnc() {
		return idMnc;
	}

	public void setIdMnc(Integer idMnc) {
		this.idMnc = idMnc;
	}

	public Integer getIdSimCardTest() {
		return idSimCardTest;
	}

	public void setIdSimCardTest(Integer idSimCardTest) {
		this.idSimCardTest = idSimCardTest;
	}

	public Integer getIdSapProductCode() {
		return idSapProductCode;
	}

	public void setIdSapProductCode(Integer idSapProductCode) {
		this.idSapProductCode = idSapProductCode;
	}

	public Integer getIdIccidPattern() {
		return idIccidPattern;
	}

	public void setIdIccidPattern(Integer idIccidPattern) {
		this.idIccidPattern = idIccidPattern;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	public Integer getIdDenatranLotOrder() {
		return idDenatranLotOrder;
	}

	public void setIdDenatranLotOrder(Integer idDenatranLotOrder) {
		this.idDenatranLotOrder = idDenatranLotOrder;
	}

	public Integer getIdRemoteEnableLotOrder() {
		return idRemoteEnableLotOrder;
	}

	public void setIdRemoteEnableLotOrder(Integer idRemoteEnableLotOrder) {
		this.idRemoteEnableLotOrder = idRemoteEnableLotOrder;
	}

	public Integer getOpc() {
		return opc;
	}

	public void setOpc(Integer opc) {
		this.opc = opc;
	}

	public Integer getCard() {
		return card;
	}

	public void setCard(Integer card) {
		this.card = card;
	}

	public Integer getVirtualNetwork() {
		return virtualNetwork;
	}

	public void setVirtualNetwork(Integer virtualNetwork) {
		this.virtualNetwork = virtualNetwork;
	}

	public Integer getCombo() {
		return combo;
	}

	public void setCombo(Integer combo) {
		this.combo = combo;
	}

	public Integer getSerialRecharge() {
		return serialRecharge;
	}

	public void setSerialRecharge(Integer serialRecharge) {
		this.serialRecharge = serialRecharge;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getIdInstances() {
		return idInstances;
	}

	public void setIdInstances(Integer idInstances) {
		this.idInstances = idInstances;
	}

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

}

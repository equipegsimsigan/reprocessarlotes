package br.com.indracompany.reprocessarlotes.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class RemoteEnableLotOrder {
	@Id
	@Column(name = "id_remote_enable_lot_order")
	private Integer idRemoteEnableLotOrder;

	private Integer imsi;

	private long iccid;

	@Column(name = "user_new")
	private String userNew;

	private String ip;

	@Column(name = "date_new")
	private LocalDateTime dateNew;

	@Column(name = "id_remote_enable_order")
	private Integer idRemoteEnableOrder;

	@Column(name = "codigo_plano")
	private Integer codigoPlano;

	@Column(name = "batch_vts")
	private Integer batchVts;

	private LocalDateTime quantity;

	public Integer getIdRemoteEnableLotOrder() {
		return idRemoteEnableLotOrder;
	}

	public void setIdRemoteEnableLotOrder(Integer idRemoteEnableLotOrder) {
		this.idRemoteEnableLotOrder = idRemoteEnableLotOrder;
	}

	public Integer getImsi() {
		return imsi;
	}

	public void setImsi(Integer imsi) {
		this.imsi = imsi;
	}

	public long getIccid() {
		return iccid;
	}

	public void setIccid(long iccid) {
		this.iccid = iccid;
	}

	public String getUserNew() {
		return userNew;
	}

	public void setUserNew(String userNew) {
		this.userNew = userNew;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public LocalDateTime getDateNew() {
		return dateNew;
	}

	public void setDateNew(LocalDateTime dateNew) {
		this.dateNew = dateNew;
	}

	public Integer getIdRemoteEnableOrder() {
		return idRemoteEnableOrder;
	}

	public void setIdRemoteEnableOrder(Integer idRemoteEnableOrder) {
		this.idRemoteEnableOrder = idRemoteEnableOrder;
	}

	public Integer getCodigoPlano() {
		return codigoPlano;
	}

	public void setCodigoPlano(Integer codigoPlano) {
		this.codigoPlano = codigoPlano;
	}

	public Integer getBatchVts() {
		return batchVts;
	}

	public void setBatchVts(Integer batchVts) {
		this.batchVts = batchVts;
	}

	public LocalDateTime getQuantity() {
		return quantity;
	}

	public void setQuantity(LocalDateTime quantity) {
		this.quantity = quantity;
	}

}
